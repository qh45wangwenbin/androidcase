package com.example.app;


import com.example.app.exceptions.NonComparableElementException;

/**
 * LinkedOrderedList represents a singly linked implementation of an 
 * ordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedOrderedList<T> extends LinkedList<T>
         implements OrderedListADT<T> {
    /**
     * Creates an empty list.
     */
    public LinkedOrderedList() {
        super();
    }

    /**
     * Adds the specified element to this list at the location determined by
     * the element's natural ordering. Throws a NonComparableElementException
     * if the element is not comparable.
     *
     * @param element the element to be added to this list
     * @throws NonComparableElementException if the element is not comparable
     */
    @Override
    public void add(T element) {
        if (!(element instanceof Comparable))
            throw new NonComparableElementException("OrderedList");

        Comparable<T> comparableElement = (Comparable<T>) element;
        // find the insertion location
        LinearNode temp = head;
        LinearNode node = new LinearNode(element);
        LinearNode last = null;
        while (temp != null && comparableElement.compareTo((T) temp.getElement()) > 0) {

            last = temp;
            temp = temp.getNext();
        }


        if (last == null) {
            head = node;
        } else {
            last.setNext(node);
        }
        node.setNext(temp);
        count++;
        modCount++;
    }
}