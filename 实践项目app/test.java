package com.example.app;

public class test {
    public static void main(String[] args) {
        LinkedOrderedList a=new LinkedOrderedList();
        System.out.println("一、有序列表的插入，删除等方法的测试");
        Product pinapple=new Product("pinapple",20,6,3);
        a.add(new Product("apple",3,2,1));
        a.add(new Product("apple",2,1,1));
        a.add(new Product("queue",2,5,2));
        a.add(new Product("queue",2,4,1));
        a.add(pinapple);
        System.out.println("输出列表后：");
        System.out.println(a.toString());
        System.out.println("当前列表中储存的元素个数：");
        System.out.println(a.size());
        System.out.println("用find方法进行判断列表中是否存在指定元素（pineapple）");
        System.out.println(a.find(pinapple));
        System.out.println();
        System.out.println("删除pineapple元素");
        a.remove(pinapple);
        System.out.println(a.toString());
        System.out.println("删除后列表中的元素个数");
        System.out.println(a.size());
        System.out.println("用find方法进行判断列表中是否存在指定元素（pineapple）");
        System.out.println(a.find(pinapple));
        System.out.println("因为该列表为有序列表，所以再此方法中不进行对于排序");

        System.out.println("二、无序列表的插入");
        Product apple1 = new Product("apple",3,4,6);
        Product apple2=new Product("apple",4,6,2);
        Product apple3=new Product("apple",5,2,9);
        Product banana1=new Product("banana",3,6,2);
        Product banana2 = new Product("banana",6,4,7);
        Product pineapple1=new Product("pinana",2,5,2);
        LinkedUnorderedList b =new LinkedUnorderedList();
        System.out.println("进行头插：");
        b.addToFront(apple1);
        b.addToFront(apple2);
        b.addToFront(banana1);
        System.out.println(b.toString());
        System.out.println("进行尾插：");
        b.addToRear(pinapple);
        System.out.println("无序列表中元素个数：");
        System.out.println(b.toString());
        System.out.println("用find方法进行判断列表中是否存在指定元素（pineapple）");
        System.out.println(b.find(pinapple));
        System.out.println("删除pineapple元素");
        b.remove(pinapple);
        System.out.println(b.toString());
        System.out.println("用find方法进行判断列表中是否存在指定元素（pineapple）");
        System.out.println(b.find(pinapple));
        System.out.println("删除后列表中的元素个数");
        System.out.println(b.size());
        System.out.println("对于商品的价格进行选择排序：");
        b.selectionSort();
        System.out.println(b.toString());

    }

}
