package com.example.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    LinkedUnorderedList q=new LinkedUnorderedList();
    Product apple = new Product("apple",1,2,3);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button1=(Button)findViewById(R.id.button);
        button1.setOnClickListener(new mybuttoninsert1());
        Button button2=(Button)findViewById(R.id.button2);
        button2.setOnClickListener(new mybuttoninsert2());
        Button button3=(Button)findViewById(R.id.button8);
        button3.setOnClickListener(new mybuttoninsert3());
        Button button4=(Button)findViewById(R.id.button9);
        button4.setOnClickListener(new mybuttoninsert4());
        Button button5=(Button)findViewById(R.id.button10);
        button5.setOnClickListener(new mybuttoninsert5());
        Button button6=(Button)findViewById(R.id.button4);
        button6.setOnClickListener(new mybuttoninsert6());
    }
    public class mybuttoninsert1 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a =(EditText) findViewById(R.id.editText);
            EditText b =(EditText) findViewById(R.id.editText2);
            EditText c =(EditText) findViewById(R.id.editText3);
            EditText d =(EditText) findViewById(R.id.editText4);
            String name= a.getText().toString();
            int value=Integer.parseInt(b.getText().toString());
            int weight = Integer.parseInt(c.getText().toString());
            int producttime=Integer.parseInt(d.getText().toString());
            Product w = new Product(name,value,weight,producttime);
            q.addToFront(w);
            q.addToFront(apple);
        }
    }
    public class mybuttoninsert2 implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            EditText e =(EditText) findViewById(R.id.editText9);
           // String res=e.getText().toString();

            q.remove(apple);
        }
    }
    public class mybuttoninsert3 implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            EditText f =(EditText) findViewById(R.id.editText10);
            EditText i =(EditText) findViewById(R.id.editText7);
            String re = f.getText().toString();

                i.setText(q.find(re));
        }
    }
    public class mybuttoninsert4 implements View.OnClickListener {

        @Override
        public void onClick(View v) {
             EditText g =(EditText) findViewById(R.id.editText5);
           int a = q.size();
           g.setText(String.valueOf(a));
        }
    }
    public class mybuttoninsert5 implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            TextView h = findViewById(R.id.textView9);
           String a =q.toString();
           h.setText(a);
        }
    }
    public class mybuttoninsert6 implements View.OnClickListener{

        @Override
        public void onClick(View v) {
             TextView h = findViewById(R.id.textView10);
            q.selectionSort();
            h.setText(q.toString());
        }
    }
}
