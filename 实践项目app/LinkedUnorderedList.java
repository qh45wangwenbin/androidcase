package com.example.app;



/**
 * LinkedUnorderedList represents a singly linked implementation of an 
 * unordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedUnorderedList<T> extends LinkedList<T>
         implements UnorderedListADT<T> {
    /**
     * Creates an empty list.
     */

    public LinkedUnorderedList() {
        super();
    }

    /**
     * Adds the specified element to the front of this list.
     *
     * @param element the element to be added to the list
     */
    @Override
    public void addToFront(T element) {
        LinearNode temp = head;
        LinearNode node = new LinearNode(element);
        node.setNext(temp);
        head = node;
        count++;
    }

    /**
     * Adds the specified element to the rear of this list.
     *
     * @param element the element to be added to the list
     */
    @Override
    public void addToRear(T element) {
        LinearNode temp = head;
        LinearNode node = new LinearNode(element);

        while (temp.getNext() != null) {
            temp = temp.getNext();
        }
        temp.setNext(node);
        count++;
    }

    public Product fff(String element){
        LinearNode<Product> current= (LinearNode<Product>) head;
        boolean found=false;
        while (current != null && !found) {
            if (element.equals(current.getElement().getName())) {
                return current.getElement();
            } else {
                current = current.getNext();
            }
        }
        return null;
    }


    @Override
    public void addAfter(T element, T target) {
        LinearNode node = new LinearNode(element);
        LinearNode temp = head;
        for (int i = 0; i < Integer.parseInt(String.valueOf(element)); i++) {
            temp = temp.getNext();
        }
        node.setNext(temp.getNext().getNext());
        temp.setNext(node);
        count++;
    }

    public String find(String name) {
        boolean found = false;
        //chap06.实践.LinearNode<T> previous = null;
        LinearNode<Product> current = (LinearNode<Product>) head;
        while (current != null && !found) {
            if (name.equals(current.getElement().getName())) {
                found = true;
                break;
            } else {
                current = current.getNext();
            }
        }
        if (found==true){
            return "是";
        }
        else {
            return "否";
        }

    }


    public void selectionSort() {
        LinearNode<Product> temp = (LinearNode<Product>) head;
        LinearNode<Product> node1, node2, node3;
        node1 = node2 = node3 = null;
        Product min = new Product("", 0, 0, 0);
        while (temp.getNext() != null) {
            node2 = null;
            node3 = node1 = temp;
            while (node1.getNext() != null) {
                if (node1.getNext().getElement().getValue()<node3.getElement().getValue()) {
                    node3 = node2 = node1.getNext();
                    min = node1.getNext().getElement();
                }
                node1 = node1.getNext();
            }
            if (node2 != null) {
                node2.setElement(temp.getElement());
                temp.setElement(min);
            }
            temp = temp.getNext();
        }

    }

}


