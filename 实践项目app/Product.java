package com.example.app;

public class Product implements Comparable<Product> {
    private String name;
    private int value;
    private int weight;
    private int producetime;

    public Product(String name, int value, int weight, int producetime) {
        this.name = name;
        this.value = value;
        this.weight = weight;
        this.producetime = producetime;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public int getWeight() {
        return weight;
    }

    public int getProducetime() {
        return producetime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setProducetime(int producetime) {
        this.producetime = producetime;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", weight=" + weight +
                ", producetime=" + producetime +
                '}';
//        return name;
    }

    @Override
    public int compareTo(Product o) {
        if (name.compareTo(o.name)>0){
            return 1;
        }
        if (name.compareTo(o.name)<0){
            return -1;
        }
        if (name.compareTo(o.name)==0){
            if (value>o.value){
                return 1;
            }
            if (value<o.value){
                return -1;
            }
            if (value==o.value){
                if (weight>o.weight){
                    return 1;
                }
                if (weight<o.weight){
                    return -1;
                }
                if (weight==o.weight){
                    if (producetime>o.producetime){
                        return 1;
                    }
                    if (producetime<o.producetime){
                        return -1;
                    }
                }
            }
        }
        return 0;
    }
}
