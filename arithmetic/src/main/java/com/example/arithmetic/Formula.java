package com.example.arithmetic;

public class Formula {
    private String str, string;
    private int s1, s2, f;
    protected int Level;
    private int num;


    public Formula(){
        str = "";
        string = "";
        s1 = 0;
        s2 = 0;
        f = 0;
        Level = 1;
    }

    public String MakeFormula(int level){
        Level = level;
        s1 = (int)(Math.random() * 100)+1;
        s2 = (int)(Math.random() * 100)+1;
        f = (int)(Math.random() * 4);
        switch (f){
            case 0:{
                str = " ＋ ";
                break;
            }
            case 1:{
                str = " － ";
                break;
            }
            case 2:{
                str = " × ";
                break;
            }
            case 3:{
                str = " ÷ ";
            }
        }
            int kuo = (int) (Math.random() * 2);
        if (level == 1)
            kuo = 0;
            if (str == "＋" || str == "－") {
                if (kuo == 1)
                    str = "(" + s1 + str + s2 + ")";
                else
                    str = s1 + str + s2;
            } else
                str = s1 + str + s2;

        for (int i = 0; i < level - 1; i++){//循环，将一级式添加为多级
            int a = (int) (Math.random()*100)+1;
            int t = (int) (Math.random()*4);
            int arrow = (int)(Math.random()*2);
            String s = "";
                    switch (t) {
                        case 0: {
                            s = " ＋ ";
                            break;
                        }
                        case 1: {
                            s = " － ";
                            break;
                        }
                        case 2: {
                            s = " × ";
                            break;
                        }
                        case 3: {
                            s = " ÷ ";
                        }
                    }
                    switch (arrow) {
                        case 0:
                        str = a + s + str;
                        break;
                        case 1:
                        str = str + s + a;
                    }
            }
        return str;
    }

    public Formula(int num)
    {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    @Override
    public String toString() {
        String result = "" + this.num;
        return result;
    }

    public Formula add(Formula op2)
    {
        int common = this.num + op2.getNum();
        System.out.print("(" + this.toString() + ")" + "+" + "(" + op2.toString() + ")" + " ＝ " + "\n");
        return new Formula(common);
    }

    public Formula subtract(Formula op2)
    {
        int common = this.num - op2.getNum();
        System.out.print("(" + this.toString() + ")" + "-" + "(" + op2.toString() + ")" + " ＝ " + "\n");
        return new Formula(common);
    }

    public Formula multiply(Formula op2)
    {
        int common = this.num * op2.getNum();
        System.out.print("(" + this.toString() + ")" + "*" + "(" + op2.toString() + ")" + " ＝ " + "\n");
        return new Formula(common);
    }

    public Formula divide(Formula op2)
    {
        int common = this.num / op2.getNum();
        System.out.print("(" + this.toString() + ")" + "/" + "(" + op2.toString() + ")" + " ＝ " + "\n");
        return new Formula(common);
    }

}
