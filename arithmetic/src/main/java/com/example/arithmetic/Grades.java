package com.example.arithmetic;

import java.util.Random;

/**
 * Grades class
 *
 * @author 唐才铭
 * @date 2018/05/01
 */
public class Grades {
    //  声明变量：分子，分母
    private int numerator, denominator;

    //  构造函数
    public Grades(int numerator, int denominator)
    {
        if(denominator == 0) {
            denominator = 1;
        }

        if(denominator < 0)
        {
            numerator = numerator * -1;
            denominator = denominator * -1;
        }

        this.numerator = numerator;
        this.denominator = denominator;

        reduce();
    }

    public int getNumerator()
    {
        return this.numerator;
    }

    public int getDenominator()
    {
        return this.denominator;
    }

    //  加
    public Grades add(Grades op2)
    {
        int commonDenominator = this.denominator * op2.getDenominator();
        int numerator1 = this.numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int sum = numerator1 + numerator2;

        return new Grades(sum,commonDenominator);
    }

    //  减
    public Grades subtract(Grades op2)
    {
        int commonDenominator = this.denominator * op2.getDenominator();
        int numerator1 = this.numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int different = numerator1 - numerator2;

        return new Grades(different,commonDenominator);
    }

    //  乘
    public Grades multiply(Grades op2)
    {
        int numer = this.numerator * op2.getNumerator();
        int denom = this.denominator * op2.getDenominator();

        return new Grades(numer,denom);
    }

    //   除
    public Grades divide(Grades op2)
    {
        int numer = this.numerator * op2.getDenominator();
        int denom = this.denominator * op2.getNumerator();

        return new Grades(numer,denom);
    }

    public boolean isLike(Grades op2)
    {
        return (this.numerator == op2.getNumerator() &&
                 this.denominator == op2.getDenominator());
    }

    @Override
    public String toString()
    {
        String result;
        if (this.numerator == 0) {
            result = "0";
        } else
            if (this.denominator == 1) {
                result = this.numerator + "";
            } else {
                result = this.numerator + "/" + this.denominator;
            }

        return result;
    }

    private void reduce()
    {

            if (this.numerator != 0) {
                int common = gcd(Math.abs(this.numerator), this.denominator);

                this.numerator = this.numerator / common;
                this.denominator = this.denominator / common;
            }


    }

    private int gcd(int num1, int num2)
    {
        while (num1 != num2) {
            if (num1 > num2) {
                num1 = num1 - num2;
            } else {
                num2 = num2 - num1;
            }
        }

        return num1;
    }

    //  静态方法获取一个真分数
    public static Grades obj(){
        Random generator = new Random();
        int a = generator.nextInt(51),b= generator.nextInt(51);
        while ((a-b)>=0)  // 保证真分数
        {
            a = generator.nextInt(51);
            b= generator.nextInt(51);
        }
        return new Grades(a , b);
    }
}
