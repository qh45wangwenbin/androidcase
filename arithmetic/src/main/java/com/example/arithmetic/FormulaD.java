package com.example.arithmetic;

public class FormulaD {
    private String str, string;
    private int f;
    private Grades g1, g2;
    protected int Level;

    public FormulaD(){
        str = "";
        string = "";
        g1 = Grades.obj();
        g2 = Grades.obj();
        f = 0;
        Level = 1;
    }

    public String MakeFormulaD(int level){
        Level = level;

        f = (int)(Math.random() * 4);
        switch (f){
            case 0:{
                str = "＋";
                break;
            }
            case 1:{
                str = "－";
                break;
            }
            case 2:{
                str = "×";
                break;
            }
            case 3:{
                str = "÷";
            }
        }

        int kuo = (int)(Math.random() * 2);
        if (str == "＋" || str == "－"){
            if (kuo == 1)
                str = g1 + str + g2;
            else
                str = g1 + str + g2;
        }
        else
            str = g1 + str + g2;

        for (int i = 0; i < level - 1; i++){//循环，将一级式添加为多级
            Grades a = Grades.obj();
            int t = (int) (Math.random()*4);
            int arrow = (int)(Math.random()*2);
            String s = "";
            switch (t) {
                case 0: {
                    s = "＋";
                    break;
                }
                case 1: {
                    s = "－";
                    break;
                }
                case 2: {
                    s = "×";
                    break;
                }
                case 3: {
                    s = "÷";
                }
            }
            switch (arrow) {
                case 0:
                    str = a + s + str;
                    break;
                case 1:
                    str = str + s + a;
            }
        }
        return str;
    }

}

