package com.example.arithmetic;

import java.util.Scanner;
import java.text.DecimalFormat;

public class caogao {
    public static void main(String[] args) {
        String fo = "";
        int win = 0;
        int lose = 0;
        DecimalFormat df = new DecimalFormat("0.00%");
        df.setMaximumFractionDigits(1);

        System.out.println("整数计算（1），分数运算（2）：");
        Scanner scan = new Scanner(System.in);
        int option1 = scan.nextInt();

        System.out.println("几级运算？");
        int level = scan.nextInt();

        System.out.println("多少题目？");
        int Tnum = scan.nextInt();
        for (int i = 0;i < Tnum; i++) {
            if (option1 == 1) {
                Formula f = new Formula();
                fo = f.MakeFormula(level);
            } else if (option1 == 2) {
                FormulaD d = new FormulaD();
                fo = d.MakeFormulaD(level);
            }

            String stresult = "";

            System.out.println(fo + "=");
            if (option1 == 1) {
                Calculate ca = new Calculate();
                int result = ca.suanshu(ca.parse(ca.zb(fo)));
                stresult = String.valueOf(result);

            }
            else if (option1 == 2){
                Transvertor tv = new Transvertor();
                GradeCalculate gc = new GradeCalculate();
                stresult = gc.getResult2(tv.getAnswer(fo));
            }

            System.out.println("你的结果是：");
            String res1 = scan.next();

            if (res1.equals(stresult))
            {
                System.out.println("答对了！");
                win++;
            }

            else {
                System.out.println("错误。正确答案是：" + stresult);
                lose++;
            }
        }

        double num1=(win * 1.0)/(Tnum * 1.0);
        System.out.println("您一共做了"+ Tnum +"道题目，做对"+ win +"道，您的正确率为： "+df.format(num1));
    }
}
