package com.example.arithmetic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {
    Stack<String> abc = new Stack();//操作数栈
    Stack<String> bcd = new Stack();//操作符栈
    Stack<Integer> stack = new Stack<Integer>();
    List<String> list = new ArrayList<String>();
    private int count=0;
    public String[] strOp = {"＋","－","×","÷","(",")"};
    public char[] op = {'＋','－','×','÷','(',')'};
    public boolean isOp(String s){
        for(int i=0;i<strOp.length;i++){
            if(strOp[i].equals(s)){
                return true;
            }
        }
        return false;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, "Hello sir!", Toast.LENGTH_LONG).show();
        Button button1 = (Button) findViewById(R.id.button);
        Button button2 = (Button) findViewById(R.id.button2);
        Button button3 = (Button) findViewById(R.id.button3);
        Button button4 = (Button) findViewById(R.id.button5);
        Button button5 = (Button) findViewById(R.id.button6);
        button1.setOnClickListener(new mybuttoninsert1());
        button2.setOnClickListener(new mybuttoninsert2());
        button3.setOnClickListener(new mybuttoninsert3());
        button4.setOnClickListener(new mybuttoninsert4());
        button5.setOnClickListener(new mybuttoninsert5());
    }

    public class mybuttoninsert1 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a = (EditText) findViewById(R.id.editText2);
           // EditText b = (EditText) findViewById(R.id.editText5);
            Formula aa = new Formula();
            String str =aa.MakeFormula(3);
            String str2 = str.replaceAll(" ", "");
            a.setText(str2,TextView.BufferType.EDITABLE);
        }
    }

    public class mybuttoninsert2 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a = (EditText) findViewById(R.id.editText2);
            a.getText();
            jisuan2 b=new jisuan2();
            EditText c = (EditText) findViewById(R.id.editText3);
            c.setText(b.parse(b.zb(a.getText().toString())),TextView.BufferType.EDITABLE);
        }
    }

    public class mybuttoninsert3 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a = (EditText) findViewById(R.id.editText3);
            EditText c = (EditText) findViewById(R.id.editText2);
            a.getText();
            Calculate ab = new Calculate();
            EditText b = (EditText) findViewById(R.id.editText4);
                String result = String.valueOf(ab.suanshu(ab.parse(ab.zb(c.getText().toString()))));
            b.setText(result,TextView.BufferType.EDITABLE);
        }
    }
    public class mybuttoninsert4 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a = (EditText) findViewById(R.id.editText8);
            EditText b = (EditText) findViewById(R.id.editText9);
            EditText c = (EditText) findViewById(R.id.editText2);
            EditText d = (EditText) findViewById(R.id.editText3);
            count++;
            jisuan2  p =new jisuan2();
            //for (int i=0;i<=c.getText().length();i++)
//            if (String.valueOf(c.getText().charAt(count-1)) !="＋"||String.valueOf(c.getText().charAt(count-1))!="－"||String.valueOf(c.getText().charAt(count-1))!="×"||String.valueOf(c.getText().charAt(count-1))!="÷")
//            {
//                abc.push(String.valueOf(c.getText().charAt(count-1)));
//                a.setText(abc.toString(),TextView.BufferType.EDITABLE);
//            }
//            else
//            {abc.push(String.valueOf(c.getText().charAt(count-1)));
//                b.setText(abc.toString(),TextView.BufferType.EDITABLE);
                List<String> abp=p.zb(c.getText().toString());
                String s = abp.get(count-1);
                if (s.equals("(")){
                    bcd.push(s);
                }else if (s.equals("×")||s.equals("÷")){
                    bcd.push(s);
            }else if (s.equals("＋")||s.equals("－")) {
                    if (!bcd.empty()) {
                        while (!(bcd.peek().equals("("))) {
                            abc.push(bcd.pop());
                            if (bcd.empty()) {
                                break;
                            }
                        }
                        bcd.push(s);
                    } else {
                        bcd.push(s);
                    }
                }
                else if (s.equals(")")){
                    while (!(bcd.peek().equals("("))){
                        abc.push(bcd.pop());
                    }
                    bcd.pop();
                }else {
                    abc.push(s);
                }
                if (count==abc.size()-1){
                    while (!bcd.empty()){
                        abc.push(bcd.pop());
                    }
                }
                a.setText(abc.toString(),TextView.BufferType.EDITABLE);
                b.setText(bcd.toString(),TextView.BufferType.EDITABLE);
                }
}
    public class mybuttoninsert5 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText c = (EditText) findViewById(R.id.editText2);
            Calculate a = new Calculate();
            List<String> plm=a.parse(a.zb(c.getText().toString()));
            count++;

            String s = plm.get(count-1);
            int t = 0;
            if (!isOp(s)){
                t = Integer.parseInt(s);
                stack.push(t);
            }else {
                if(s.equals("＋")){
                    int a1 = Integer.parseInt(String.valueOf(stack.pop()));
                    int a2 = Integer.parseInt(String.valueOf(stack.pop()));
                    int v = a2+a1;
                    stack.push(v);
                }else if(s.equals("－")){
                    int a1 = Integer.parseInt(String.valueOf(stack.pop()));
                    int a2 = Integer.parseInt(String.valueOf(stack.pop()));
                    int v = a2-a1;
                    stack.push(v);
                }else if(s.equals("×")){
                    int a1 = Integer.parseInt(String.valueOf(stack.pop()));
                    int a2 = Integer.parseInt(String.valueOf(stack.pop()));
                    int v = a2*a1;
                    stack.push(v);
                }else if(s.equals("÷")){
                    int a1 = Integer.parseInt(String.valueOf(stack.pop()));
                    int a2 = Integer.parseInt(String.valueOf(stack.pop()));
                    int v = a2/a1;
                    stack.push(v);
                }
            }
            EditText o = (EditText) findViewById(R.id.editText10);
            o.setText(stack.toString(),TextView.BufferType.EDITABLE);
            }

    }
}

