package com.example.arithmetic;

import java.util.Stack;
import java.util.StringTokenizer;


public class Transvertor {
    private int num1, num2, value1, value2, number;
    private Stack stack1 = new Stack();
    private String result1;
    private int[][] array = {{0, 0, 0, 0, 0,0,0},
            {0, 1, 1, -1, -1,-1,-1},
            {0, 1, 1, -1, -1,-1,-1},
            {0, 1, 1, 1, 1,1,1},
            {0, 1, 1, 1, 1,1,1},
            {0, 1, 1, 1, 1,1,1},
            {0, 1, 1, 1, 1,1,1}
    };

    public Transvertor() {
        num1 = 0;
        num2 = 0;
        value1 = 0;
        value2 = 0;
        result1=" ";

    }


    public String getAnswer(String Expression) {

        StringTokenizer st = new StringTokenizer(Expression);
        while (st.hasMoreTokens()) {
            String A = st.nextToken();

            if (A.equals("－")  || A.equals("＋") || A.equals("×") || A.equals("÷")) {
                switch (A) {
                    case "＋":
                        number = 1;
                        break;
                    case "－":
                        number = 2;
                        break;
                    case "×":
                        number = 3;
                        break;
                    case "÷":
                        number = 4;
                        break;
                    default:
                }

                if (stack1.empty()) {

                    stack1.push(A);
                    num1 = number;
                    num2 = number;
                    value1++;

                } else {

                    num1=num2;
                    num2 = number;

                    if (array[num1][num2] < 0) {
                        stack1.push(A);
                        value1 += 1;
                    } else {
                        //result1 += stack1.pop() + " ";
                        while(!stack1.empty()) {
                            result1 += stack1.pop() + " ";
                        }

                        stack1.push(A);
                    }
                }

            }
            else {
                result1 += A + " ";
            }

        }
        while(!stack1.empty()) {
            result1 += stack1.pop() + " ";
        }

        return result1;

    }

    public String getResult()
    {
        return  result1;
    }
}

