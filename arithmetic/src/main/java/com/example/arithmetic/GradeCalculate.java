package com.example.arithmetic;

import java.util.Stack;
import java.util.StringTokenizer;
public class GradeCalculate {
    String  op1,op2;
    Grades countresult,B1,B2;
    String A,result1;
    private int number,numer1,denom1,numer2,demon2;
    Stack<String> stack2;

    public GradeCalculate()
    {
        stack2= new Stack<String >();
        result1="";

    }
    public String getResult2(String result1) {
        StringTokenizer st = new StringTokenizer(result1);

        while (st.hasMoreTokens())
        {
            A = st.nextToken();

            if (A.equals("－")  || A.equals("＋") || A.equals("×") || A.equals("÷")) {

                switch (A) {
                    case "＋":
                        number = 1;
                        break;
                    case "－":
                        number = 2;
                        break;
                    case "×":
                        number = 3;
                        break;
                    case "÷":
                        number = 4;
                        break;
                    default:
                }

                op2 = stack2.pop();
                op1 = stack2.pop();//注意！！

                StringTokenizer st1 = new StringTokenizer(op1, "/");
                numer1 = Integer.parseInt(st1.nextToken());
                if (st1.hasMoreTokens()) {
                    denom1 = Integer.parseInt(st1.nextToken());
                }
                else {
                    denom1 = 1;
                }
                B1 = new Grades(numer1, denom1);

                StringTokenizer st2 = new StringTokenizer(op2, "/");
                numer2 = Integer.parseInt(st2.nextToken());
                if (st2.hasMoreTokens()) {
                    demon2 = Integer.parseInt(st2.nextToken());
                } else {
                    demon2 = 1;
                }
                B2 = new Grades(numer2, demon2);

                countresult=calculate(number,B1,B2);
                stack2.push(countresult.toString());

            }
            else {
                stack2.push(A);
            }
        }
        result1 =stack2.pop();
        return result1;
    }

    public Grades calculate(int operation, Grades B1, Grades B2) {

        Grades result2 = null;
        switch (operation)
        {
            case 1:
                result2 = B1.add(B2);
                break;
            case 2:
                result2 = B1.subtract(B2);
                break;
            case 3:
                result2 = B1.multiply(B2);
                break;
            case 4:
                result2 = B1.divide(B2);
                break;
            default:
        }

        return result2;

    }
}