package com.example.sorting_and_searching;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sorting_and_searching.Sorting.Sorting;

import java.util.Arrays;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Button button1=(Button)findViewById(R.id.button);
        button1.setOnClickListener(new mybuttoninsert1());
        Button button2=(Button)findViewById(R.id.button2);
        button2.setOnClickListener(new mybuttoninsert2());
        Button button3=(Button)findViewById(R.id.button3);
        button3.setOnClickListener(new mybuttoninsert3());

        Button button5=(Button)findViewById(R.id.button5);
        button5.setOnClickListener(new mybuttoninsert5());

    }
    public class mybuttoninsert1 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a =(EditText) findViewById(R.id.editText);
            String[] num=a.getText().toString().split(",");
            Integer[] temp = new Integer[num.length];
            for (int i =0;i<num.length;i++){
                temp[i]=Integer.parseInt(num[i]);
            }
            EditText b =(EditText) findViewById(R.id.editText2);
            Sorting c = new Sorting();
            c.selectionSort(temp);
            b.setText(Arrays.toString(temp));
        }
    }
    public class mybuttoninsert2 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a =(EditText) findViewById(R.id.editText);
            String[] num=a.getText().toString().split(",");
            Integer[] temp = new Integer[num.length];
            for (int i =0;i<num.length;i++){
                temp[i]=Integer.parseInt(num[i]);
            }
            EditText b =(EditText) findViewById(R.id.editText3);
            Sorting c = new Sorting();
            c.insertionSort(temp);
            b.setText(Arrays.toString(temp));
        }
    }
    public class mybuttoninsert3 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a =(EditText) findViewById(R.id.editText);
            String[] num=a.getText().toString().split(",");
            Integer[] temp = new Integer[num.length];
            for (int i =0;i<num.length;i++){
                temp[i]=Integer.parseInt(num[i]);
            }
            EditText b =(EditText) findViewById(R.id.editText4);
            Sorting c = new Sorting();
            c.bubbleSort(temp);
            b.setText(Arrays.toString(temp));
        }
    }

    public class mybuttoninsert5 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Intent intent =new Intent(SecondActivity.this,thirdAicivity.class);
            startActivity(intent);

        }
    }
}
