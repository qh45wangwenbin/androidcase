package com.example.sorting_and_searching;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sorting_and_searching.Search.Searching;

public class thirdAicivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Button button1=(Button)findViewById(R.id.button6);
        button1.setOnClickListener(new mybuttoninsert1());
        Button button2=(Button)findViewById(R.id.button7);
        button2.setOnClickListener(new mybuttoninsert2());
        Button button3=(Button)findViewById(R.id.button8);
        button3.setOnClickListener(new mybuttoninsert3());
        Button button4=(Button)findViewById(R.id.button9);
        button4.setOnClickListener(new mybuttoninsert4());
        Button button5=(Button)findViewById(R.id.button10);
        button5.setOnClickListener(new mybuttoninsert5());

    }
    public class mybuttoninsert1 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a =(EditText) findViewById(R.id.editText6);
            String[] num=a.getText().toString().split(",");
           int[] temp = new int[num.length];
            for (int i =0;i<num.length;i++){
                temp[i]=Integer.parseInt(num[i]);
            }
            EditText b =(EditText) findViewById(R.id.editText7);
            int target=Integer.parseInt(b.getText().toString());
            Searching c =new Searching();
            EditText d =(EditText) findViewById(R.id.editText8);
            if (c.InsertionSearch(temp,target)){
                d.setText("成功找到");
            }
            else {
                d.setText("没有找到");
            }

        }
    }
    public class mybuttoninsert2 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a =(EditText) findViewById(R.id.editText6);
            String[] num=a.getText().toString().split(",");
            int[] temp = new int[num.length];
            for (int i =0;i<num.length;i++){
                temp[i]=Integer.parseInt(num[i]);
            }
            EditText b =(EditText) findViewById(R.id.editText11);
            int target=Integer.parseInt(b.getText().toString());
            Searching c =new Searching();
            EditText d =(EditText) findViewById(R.id.editText14);
            if (c.fibonacciSearch(temp,target)){
                d.setText("成功找到");
            }
            else {
                d.setText("没有找到");
            }

        }
    }
    public class mybuttoninsert3 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a =(EditText) findViewById(R.id.editText6);
            String[] num=a.getText().toString().split(",");
            Integer[] temp = new Integer[num.length];
            for (int i =0;i<num.length;i++){
                temp[i]=Integer.parseInt(num[i]);
            }
            EditText b =(EditText) findViewById(R.id.editText12);
            int target=Integer.parseInt(b.getText().toString());
            Searching c =new Searching();
            EditText d =(EditText) findViewById(R.id.editText15);
            if (c.linearSearch(temp,0,temp.length,target)){
                d.setText("成功找到");
            }
            else {
                d.setText("没有找到");
            }
        }
    }

    public class mybuttoninsert4 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText a =(EditText) findViewById(R.id.editText6);
            String[] num=a.getText().toString().split(",");
            Integer[] temp = new Integer[num.length];
            for (int i =0;i<num.length;i++){
                temp[i]=Integer.parseInt(num[i]);
            }
            EditText b =(EditText) findViewById(R.id.editText13);
            int target=Integer.parseInt(b.getText().toString());
            Searching c =new Searching();
            EditText d =(EditText) findViewById(R.id.editText16);
            if (c.binarySearch(temp,0,temp.length,target)){
                d.setText("成功找到");
            }
            else {
                d.setText("没有找到");
            }
        }
    }




    public class mybuttoninsert5 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Intent intent =new Intent(thirdAicivity.this,fourthActivity.class);
            startActivity(intent);
        }
    }
}