package com.example.infixsuffix;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this,"Hello sir!",Toast.LENGTH_LONG).show();
        Button button1=(Button)findViewById(R.id.button);
        Button button2=(Button)findViewById(R.id.button2);
        button1.setOnClickListener(new mybuttoninsert1());
        button2.setOnClickListener(new mybuttoninsert2());
    }
    public class mybuttoninsert1 implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            EditText b = (EditText) findViewById(R.id.editText2);
            b.getText();
            jisuan2 a=new jisuan2();
            EditText c = (EditText) findViewById(R.id.editText3);
            c.setText(a.parse(a.zb(b.getText().toString())), TextView.BufferType.EDITABLE);
        }
    }
    public class mybuttoninsert2 implements View.OnClickListener{
        @Override
        public void onClick(View view) {
            EditText b = (EditText) findViewById(R.id.editText2);
            b.getText();
                Calculate a=new Calculate();
            EditText c = (EditText) findViewById(R.id.editText4);
            int result=a.suanshu(a.parse(a.zb(b.getText().toString())));
            String stresult = String.valueOf(result);
            System.out.println(stresult);
            c.setText(stresult, TextView.BufferType.EDITABLE);
        }
    }
}
