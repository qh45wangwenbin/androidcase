package com.example.basiccomponents;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this,"20172329",Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it 
        // is present. 
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }
//    public void ToastDemo(final String text) {
//        this.runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                Toast.makeText(MainActivity.this, text, Toast.LENGTH_LONG).show();
//            }
//        });
//    }
//    public MainActivity(android.content.Context context){
//        Toast toast=Toast.makeText(getApplicationContext(), "自定义显示位置的Toast", Toast.LENGTH_SHORT);
//        toast.show();
//     //   Toast.makeText(this,"TEST",Toast.LENGTH_LONG).show();
//    }
}